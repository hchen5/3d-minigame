using PlayerNS;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    [SerializeField]
    private PlayerBehaviour _Player;
    private int _Health;
    [SerializeField]
    private TextMeshProUGUI _HealthT;
    private void Start()
    {
        _Health = _Player.Health;
        UpdateUI();
    }
    public void UpdateHealth() 
    {
        if (_Health - 1 <= 0)
        {
            _Health = 0;
        }
        else
            _Health -= 1;
        UpdateUI();
    }
    public void UpdateUI() 
    {
        _HealthT.SetText("Vida : " + _Health);
    }
}
