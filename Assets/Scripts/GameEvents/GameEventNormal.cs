using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "GameEventNormal", menuName = "GameEvent/GameEventNormal")]
public class GameEventNormal : GameEvent {}
