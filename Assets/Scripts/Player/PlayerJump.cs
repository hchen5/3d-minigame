using FSMState;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.Timeline.Actions;
using UnityEngine;
using UnityEngine.InputSystem;

namespace PlayerNS
{
    public class PlayerJump : MBState
    {
        private PlayerBehaviour _Player;
        private Rigidbody _RigidBody;
        private Animator _Animator;
        private NewFiniteStateMachine _StateMachine;
        [SerializeField]
        private float _JumpForce; 
        private void Awake()
        {
            _StateMachine = GetComponent<NewFiniteStateMachine>();
            _Animator = GetComponent<Animator>();
            _RigidBody = GetComponent<Rigidbody>();
            _Player = GetComponent<PlayerBehaviour>();
        }
        private void AttackAction(InputAction.CallbackContext obj)
        {
            _StateMachine.ChangeState<PlayerAttack>();
        }
        public override void Init()
        {
            base.Init();
                JumpForce();
                _Player.Input.FindAction("Attack").started += AttackAction;
        }
        public override void Exit()
        {
            if(_Player != null)
                _Player.Input.FindAction("Attack").started += AttackAction;
            base.Exit();
        }
        /// <summary>
        /// Add force to rigidbody in the vector y
        /// </summary>
        /// <param name="obj"></param>
        private void JumpForce() 
        {
            _RigidBody.AddForce(Vector3.up * _JumpForce ,ForceMode.Impulse);
            _StateMachine.ChangeState<PlayerMovement>();
        }
    }
}
