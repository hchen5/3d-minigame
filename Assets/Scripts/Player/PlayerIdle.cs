using FSMState;
using PlayerNS;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace PlayerNS
{
    public class PlayerIdle : MBState
    {
        private PlayerBehaviour _Player;
        private Rigidbody _RigidBody;
        private Animator _Animator;
        private NewFiniteStateMachine _StateMachine;

        private void Awake()
        {
            _StateMachine = GetComponent<NewFiniteStateMachine>();
            _Animator = GetComponent<Animator>();
            _RigidBody = GetComponent<Rigidbody>();
            _Player = GetComponent<PlayerBehaviour>();
        }
        /*
        private void JumpAction(InputAction.CallbackContext obj)
        {
            if (_Player.IsGrounded()) 
            {
                _StateMachine.ChangeState<PlayerJump>();
            }
        }*/
        private void AttackAction(InputAction.CallbackContext obj)
        {
            _StateMachine.ChangeState<PlayerAttack>();
        }
        public override void Init()
        {
            base.Init();
                _Player.Input.FindAction("Jump").started += JumpForce;
                _Player.Input.FindAction("Attack").started += AttackAction;
        }
        public override void Exit()
        {
            //Desuscribe input from jumpaction
            if (_Player != null)
            {
                _Player.Input.FindAction("Jump").started -= JumpForce;
                _Player.Input.FindAction("Attack").started -= AttackAction;
            }
            base.Exit();
        }
        private void Update()
        {
            if (_Player.Movement.ReadValue<Vector2>() != Vector2.zero)
                _StateMachine.ChangeState<PlayerMovement>();
        }
        /// <summary>
        /// Add force to rigidbody in the vector y
        /// </summary>
        /// <param name="obj"></param>
        private void JumpForce(InputAction.CallbackContext obj)
        {
            _RigidBody.AddForce(Vector3.up * _Player.JumpForce, ForceMode.Impulse);
        }
    }
}