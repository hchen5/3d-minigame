using FSMState;
using NS_Enemy;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlayerNS
{
    public class PlayerAttack : MBState
    {
        private PlayerBehaviour _Player;
        private Rigidbody _RigidBody;
        private Animator _Animator;
        private NewFiniteStateMachine _StateMachine;
        [SerializeField]
        private Camera _Cam;
        [SerializeField]
        private int _BulletN;
        private void Awake()
        {
            _StateMachine = GetComponent<NewFiniteStateMachine>();
            _Animator = GetComponent<Animator>();
            _RigidBody = GetComponent<Rigidbody>();
            _Player = GetComponent<PlayerBehaviour>();
        }
        public override void Init()
        {
            base.Init();
                Raycast();
        }
        public override void Exit()
        {
            base.Exit();
        }
        /// <summary>
        /// Raycast in the camera forward and print transform name if hitted collider is not null
        /// </summary>
        private void Raycast()
        {
            RaycastHit hit;
            Physics.Raycast(_Cam.transform.position, _Cam.transform.forward, out hit, Mathf.Infinity);
            //if(_BulletN > 0)
            if (hit.collider != null)
            {
                Debug.Log(hit.transform.name);
                if (hit.transform.gameObject.layer == LayerMask.NameToLayer("Enemy"))
                {
                    if (hit.transform.GetComponent<Enemy>())
                    {
                        Debug.Log(hit.transform.name + "Raycast");
                        Enemy e = hit.transform.GetComponent<Enemy>();
                        e.EnemyHit();
                    }
                }
            }
        }
        private void Update()
        {
            if (_RigidBody.velocity == Vector3.zero)
                _StateMachine.ChangeState<PlayerIdle>();
            else
                _StateMachine.ChangeState<PlayerMovement>();
        }
    }
}