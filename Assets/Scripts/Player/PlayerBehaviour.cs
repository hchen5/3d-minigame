using FSMState;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.Windows;
using static UnityEditor.Experimental.GraphView.GraphView;

namespace PlayerNS
{
    [RequireComponent(typeof(NewFiniteStateMachine))]
    [RequireComponent(typeof(CapsuleCollider))]
    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(PlayerIdle))]
    [RequireComponent(typeof(PlayerMovement))]
    //[RequireComponent(typeof(PlayerJump))]
    [RequireComponent(typeof(PlayerAttack))]
    public class PlayerBehaviour : MonoBehaviour
    {
        private NewFiniteStateMachine _StateMachine;
        private CapsuleCollider _CapsuleCollider;
        private Rigidbody _RigidBody;
        [SerializeField]
        private InputActionAsset _InputAsset;
        private InputActionAsset _Input;
        public InputActionAsset Input => _Input;
        private InputAction _MovementAction;
        [HideInInspector]
        public InputAction Movement => _MovementAction;
        private Vector3 _Movement;
        [HideInInspector]
        public Vector3 MovementP;
        [HideInInspector]
        public InputAction _Look;
        public InputAction Look => _Look;
        [SerializeField]
        private Transform _LookAt;
        [SerializeField]
        private int _Health = 3;
        [SerializeField]
        private float _JumpForce;
        [SerializeField]
        private float _TimeToReloadGun = 1.8f;
        public float JumpForce { get => _JumpForce; }
        public float TimeToReloadGun { get => _TimeToReloadGun;}
        public int Health { get => _Health;}

        [SerializeField]
        private GameEvent _SubstractHealth;
        private void Awake()
        {
            _StateMachine = GetComponent<NewFiniteStateMachine>();
            _CapsuleCollider = GetComponent<CapsuleCollider>();
            _RigidBody = GetComponent<Rigidbody>();
            _Input = Instantiate(_InputAsset);
            _MovementAction = _Input.FindActionMap("PlayerActions").FindAction("Movement");
            _Look = _Input.FindActionMap("PlayerActions").FindAction("Look");
            _Input.FindActionMap("PlayerActions").Enable();
        }
        private void Start()
        {
            _StateMachine.ChangeState<PlayerIdle>();
        }
        private void Update()
        {
                _Movement = _MovementAction.ReadValue<Vector2>().y * _LookAt.transform.forward + _MovementAction.ReadValue<Vector2>().x * _LookAt.transform.right;
            MovementP = _Movement;
        }
        /// <summary>
        /// Check if player touch ground
        /// </summary>
        /// <returns>true or false depen if player touch ground or no</returns>
        public bool IsGrounded() 
        {
            Debug.DrawRay(transform.position, new Vector3(0,-(transform.localScale.y +0.1f ), 0), Color.red, 2f);
            if (Physics.Raycast(transform.position, Vector3.down, transform.localScale.y + 0.1f))
                return true;
            return false;
        }
        /// <summary>
        /// Less 1 point of player health if are hitted by enemy
        /// </summary>
        public void EnemyHitPlayer() 
        {
            if (_Health - 1 <= 0)
            {
                _Health = 0;
                PlayerDead();
            }
            else
                _Health -= 1;
            _SubstractHealth.Raise();
            Debug.Log("Player Health: " + _Health);
        }
        private void PlayerDead() 
        {
            Destroy(this);
            _Input.FindActionMap("PlayerActions").Disable();
            //call gameover scene or delete player
            SceneManager.LoadScene("Final");
        }
    }
}
