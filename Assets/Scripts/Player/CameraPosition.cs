using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPosition : MonoBehaviour
{
    [SerializeField]
    Transform m_CameraPosition;
    void Update()
    {
        transform.position = m_CameraPosition.transform.position;
    }
}
