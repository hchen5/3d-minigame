using PlayerNS;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Rendering.Universal;
using UnityEngine.Windows;

public class PlayerCamera : MonoBehaviour
{
    [SerializeField]
    private InputActionAsset _InputAsset;
    private InputActionAsset _Input;
    private InputAction _Look;
    [SerializeField]
    float m_Sensibility;

    [SerializeField]
    Transform m_Cam;
    [SerializeField]
    Transform m_Orientation;

    float m_MouseX;
    float m_MouseY;

    float m_xRotation;
    float m_yRotation;

    private void Awake()
    {
        _Input = Instantiate(_InputAsset);
        _Look = _Input.FindActionMap("PlayerActions").FindAction("Look");
        _Input.FindActionMap("PlayerActions").Enable();
    }
    private void Start()
    {
        
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    private void Update()
    {

        m_MouseX = _Look.ReadValue<Vector2>().x;
        m_MouseY = _Look.ReadValue<Vector2>().y;

        m_yRotation += m_MouseX * m_Sensibility * Time.deltaTime;
        m_xRotation -= m_MouseY * m_Sensibility * Time.deltaTime;

        m_xRotation = Mathf.Clamp(m_xRotation, -90f, 90f);
        m_Cam.transform.rotation = Quaternion.Euler(-m_xRotation, m_yRotation, 0);
        m_Orientation.transform.rotation = Quaternion.Euler(0, m_yRotation, 0);
    }
}
