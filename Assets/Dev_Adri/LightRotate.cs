using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class LightRotate : MonoBehaviour
{
    [SerializeField]
    private float initialRotation;
    [SerializeField]
    private float TimePerDay;
    void Start()
    {
        transform.eulerAngles = new Vector3(initialRotation, transform.eulerAngles.y, transform.eulerAngles.z);
    }
    void Update()
    {
        float rotationPerframe = (360f / TimePerDay) * (Time.deltaTime);
        transform.Rotate(Vector3.right, rotationPerframe);
    }
}
