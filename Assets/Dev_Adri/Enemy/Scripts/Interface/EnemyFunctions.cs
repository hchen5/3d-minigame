
using UnityEngine;
using UnityEngine.AI;

namespace NS_Enemy
{
    internal interface IEnemyFunctions
    {

        /// <summary>
        /// Move the Enemy Agent to a Specific location
        /// </summary>
        /// <param name="Enemy"> Enemy GameObject </param>
        /// <param name="Destination"> Specific location where Enemy is gona move </param>
        /// <returns> In case Destination isn't part of Enemy Agent NavMesh Bake, return False; Otherwise, return True </returns>
        public static bool EnemyMove(GameObject Enemy, Vector3 Destination)
        {
            NavMeshAgent Agent = Enemy.GetComponent<NavMeshAgent>();
            Agent.isStopped = false;
            Vector3 NavMeshPosition;
            if (IsNavMeshPosition(Destination, out NavMeshPosition, 1.0f, Agent.areaMask)) 
            {
                Agent.destination = NavMeshPosition;
                return true;
            }
            return true;
        }

        /// <summary>
        /// Stop the Enemy in his actual Position
        /// </summary>
        /// <param name="Enemy"> Enemy GameObject </param>
        public static void EnemyStop(GameObject Enemy)
        {
            NavMeshAgent Agent = Enemy.GetComponent<NavMeshAgent>();
            if (Enemy.activeInHierarchy && Agent.enabled && Agent.isOnNavMesh)
                Agent.isStopped = true;
        }
        
        /// <summary>
        /// Check if Player is in range to shoot.
        /// </summary>
        /// <param name="Enemy"> Enemy GameObject </param>
        /// <param name="Target"> Traget GameObject </param>
        /// <returns> If the bullet has hit </returns>
        public static bool EnemyShoot(GameObject Enemy, GameObject Target)
        {
            RaycastHit hit;
            if (Physics.Raycast(Enemy.transform.position, (Target.transform.position - Enemy.transform.position).normalized, out hit, Mathf.Infinity, LayerMask.GetMask("Player", "Obstacles")))
            {
                if (hit.transform.gameObject.layer == LayerMask.NameToLayer("Player"))
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Get a Random Point in a Specific NavmeshMask of the Area
        /// </summary>
        /// <param name="navMeshMask"> The Namvmesh Mask where you want to search </param>
        /// <param name="center"> The Center of The sphere you're gona be searching </param>
        /// <param name="range"> The Radious of The sphere you're gona be searching </param>
        /// <param name="result"> The Position of the navMesh you're looking for </param>
        /// <returns> If the Position has been finded </returns>
        public static bool NavMeshRandomPoint(int navMeshMask, Vector3 center, float range, out Vector3 result) 
        {
            for (int i = 0; i < 30; i++) {
                Vector3 randomPoint = center + Random.insideUnitSphere * range;
                Vector3 NavMeshPosition;
                if (IsNavMeshPosition(randomPoint, out NavMeshPosition, 1.0f, navMeshMask)) 
                {
                    result = NavMeshPosition;
                    return true;
                }
            }
            result = Vector3.zero;
            return false;
	    }
        
        /// <summary>
        /// See if the given Point is Part of the NavMesh
        /// </summary>
        /// <param name="sourcePosition"> The Point you're searching </param>
        /// <param name="result"> The Position of the navMesh you're looking for </param>
        /// <param name="maxDistance"> The max distance between the given Point and the NavMesh Point </param>
        /// <param name="areaMask"> The Namvmesh Mask where you want to search </param>
        /// <returns> If the Position has been finded </returns>
        public static bool IsNavMeshPosition(Vector3 sourcePosition, out Vector3 result, float maxDistance, int areaMask)
        {
            NavMeshHit hit;
			if (NavMesh.SamplePosition(sourcePosition, out hit, maxDistance, areaMask)) 
            {
				result = hit.position;
				return true;
			}
            result = Vector3.zero;
            return false;
        }
    }
}
