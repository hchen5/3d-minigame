using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;


namespace NS_Enemy
{
    [CustomEditor(typeof(SO_Enemy))]
    public class So_EnemyEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            var myScript = target as SO_Enemy;
            base.OnInspectorGUI();

            if (myScript.MyType == SO_Enemy.PatrolingType.Random)
            {
                myScript.radius = EditorGUILayout.FloatField("Radius", myScript.radius);
            }
            else
            {
                SerializedProperty _List;
                _List = serializedObject.FindProperty("MyPath");
                EditorGUILayout.PropertyField(_List, new GUIContent("My Path"));
            }
            serializedObject.ApplyModifiedProperties();
        }
    }
}
