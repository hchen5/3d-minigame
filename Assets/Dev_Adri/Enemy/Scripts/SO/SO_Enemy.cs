using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NS_Enemy
{
    [CreateAssetMenu(fileName = "SO_Enemy", menuName = "ScriptableObjects/SO_Enemy", order = 1)]
    public class SO_Enemy : ScriptableObject
    {
        [Header("Atack Settings")]
        public float AtackRecoy;
        public GameEventNormal RaiseWhenPlayerHit;
        [Header("Chase Settings")]
        public float VelocityIncrement;
        [Header("Patrol Settings")]
        public PatrolingType MyType;
        public enum PatrolingType { Random, Determinated }
        [HideInInspector]
        public List<Vector3> MyPath;
        [HideInInspector]
        public float radius;
    }
}