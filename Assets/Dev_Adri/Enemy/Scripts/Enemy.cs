using System.Collections;
using System.Collections.Generic;
using FSMState;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Rendering;
using static NS_Enemy.IEnemyFunctions;

namespace NS_Enemy
{
    [RequireComponent(typeof(NavMeshAgent))]
    [RequireComponent(typeof(NewFiniteStateMachine))]
    [RequireComponent(typeof(SM_EnemyIddle))]
    [RequireComponent(typeof(SM_EnemyPatrol))]
    [RequireComponent(typeof(SM_EnemyChasing))]
    public class Enemy : MonoBehaviour
    {
        // Components
        private NewFiniteStateMachine _StateMachine;
        private NavMeshAgent _NavMeshAgent;
        private Animator _Animator;

        // Delegate
        public delegate void Warn();
        internal Warn WhenStopped;
        public Warn WhenDied; 
        // Coroutine
        private Coroutine _CheckEnemyPathCoroutine;
        // SO
        [SerializeField]
        internal SO_Enemy _Scriptable;
        // Variables
        internal GameObject _Player;
        private Rigidbody[] RagDollRigidbodies;
        private Material _Material;

        private void Awake()
        {
            _Material = transform.Find("WorldWar_zombie").GetComponent<Renderer>().material;
            _Animator = GetComponent<Animator>();
            _NavMeshAgent = GetComponent<NavMeshAgent>();
            _StateMachine = GetComponent<NewFiniteStateMachine>();
        }

        private void Start()
        {
            RagDollRigidbodies = transform.GetComponentsInChildren<Rigidbody>();
            _StateMachine.ChangeState<SM_EnemyPatrol>();
            transform.GetComponent<CapsuleCollider>().enabled = true;
            _NavMeshAgent.isStopped = false;
            _Animator.enabled = true;
            SetRigidBodyEnabled(false);
            SetMaterialDisolveValue(0);
        }

        /// <summary>
        /// Start Checking Enemy Path Status Coroutine and Invoke <b>WhenStopped</b> Delegate when the Path is done
        /// </summary>
        internal void StartCheckingEnemyNavmeshPathStatus()
        {
            if (_CheckEnemyPathCoroutine != null) 
                StopCoroutine(_CheckEnemyPathCoroutine);
            _CheckEnemyPathCoroutine = StartCoroutine(CheckEnemyNavmeshPathStatusCoroutine());
        }

        /// <summary>
        /// Stop Checking Enemy Path Status Coroutine
        /// </summary>
        internal void StopCheckingEnemyNavmeshPathStatus()
        {
            if (_CheckEnemyPathCoroutine != null) 
                StopCoroutine(_CheckEnemyPathCoroutine);
        }

        /// <summary>
        /// Coroutine that advise when the Enemy arrives to Destination
        /// </summary>
        /// <returns> Wait 0.2f Seconds </returns>
        private IEnumerator CheckEnemyNavmeshPathStatusCoroutine() 
        {
            while (true)
            {
                if (!_NavMeshAgent.pathPending)
                    if (_NavMeshAgent.remainingDistance <= _NavMeshAgent.stoppingDistance)
                        if (!_NavMeshAgent.hasPath || _NavMeshAgent.velocity.sqrMagnitude == 0f)
                            break;
                yield return new WaitForSeconds(0.2f);
            }
            WhenStopped.Invoke();
        }

        /// <summary>
        /// Hit Enemy
        /// </summary>
        public void EnemyHit()
        {
            EnemyDead();
        }

        /// <summary>
        /// Do whatever is necessary when enemy dies
        /// </summary>
        private void EnemyDead()
        {
            _StateMachine.ChangeState<SM_EnemyIddle>();
            _NavMeshAgent.isStopped = true;
            transform.GetComponent<CapsuleCollider>().enabled = false;
            SetRigidBodyEnabled(true);
            _Animator.enabled = false;
            StartCoroutine(DeadVanish());
        }

        /// <summary>
        /// Eneable and Disable RagDoll
        /// </summary>
        /// <param name="enabled"> Set RagDoll Enabeled or not </param>
        internal void SetRigidBodyEnabled(bool enabled)
        {
            bool isKinematic = !enabled;
            foreach (Rigidbody rigidbody in RagDollRigidbodies)
            {
                rigidbody.isKinematic = isKinematic;
            }
            _Animator.enabled = !enabled;
        }

        /// <summary>
        /// Set the given value to shaderGraph Disolve Value
        /// </summary>
        /// <param name="value"> Value of Disolve in ShaderGraph </param>
        public void SetMaterialDisolveValue(float value)
        {
            _Material.SetFloat("_Dissolve", value);
        }

        /// <summary>
        /// Disolve Coroutine
        /// </summary>
        /// <returns> Wait for seconds </returns>
        private IEnumerator DeadVanish()
        {
            yield return new WaitForSeconds(3f);
            float ValDisolve = 0;
            while (true)
            {
                ValDisolve += 0.01f;
                SetMaterialDisolveValue(ValDisolve);
                yield return new WaitForSeconds(0.01f);
                if (ValDisolve > 1)
                    break;
            }
            if (gameObject.GetComponent<Pooleable>() != null)
            {
                gameObject.GetComponent<Pooleable>().ReturnToPool();
                WhenDied.Invoke();
            } else 
            {
                Destroy(gameObject);
            }
        }
        void OnEnable()
        {
            /*_StateMachine.ChangeState<SM_EnemyPatrol>();
            _NavMeshAgent.isStopped = false;
            transform.GetComponent<CapsuleCollider>().enabled = true;
            SetRigidBodyEnabled(false);
            _Animator.enabled = true;*/

            if (RagDollRigidbodies != null)
                SetRigidBodyEnabled(false);
            SetMaterialDisolveValue(0);
        }
    }
}

