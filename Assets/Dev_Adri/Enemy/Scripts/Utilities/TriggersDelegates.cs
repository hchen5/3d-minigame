using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static NS_Enemy.IEnemyFunctions;

namespace NS_Enemy
{
    public class TriggersDelegates : MonoBehaviour
    {
        // Delegate
        internal delegate void Warn(Collider other);
        internal Warn OnTriggerEnterDelegate;
        internal Warn OnTriggerExitDelegate;
        internal Warn OnTriggerStayDelegate; 
        private void OnTriggerEnter(Collider other)
        {
            OnTriggerEnterDelegate?.Invoke(other);
        }
        private void OnTriggerExit(Collider other)
        {
            OnTriggerExitDelegate?.Invoke(other);
        }
        private void OnTriggerStay(Collider other)
        {
            OnTriggerStayDelegate?.Invoke(other);
        }
    }
}

