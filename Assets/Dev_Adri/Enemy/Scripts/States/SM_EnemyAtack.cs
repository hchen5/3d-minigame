using System.Collections;
using System.Collections.Generic;
using FSMState;
using UnityEngine;
using static NS_Enemy.IEnemyFunctions;

namespace NS_Enemy
{
    public class SM_EnemyAtack : MBState
    {
        // Components
        private NewFiniteStateMachine _StateMachine;
        private Animator _Animator;
        // SO
        private SO_Enemy _Scriptable;
        private GameObject _Player;
        // Coroutine
        private Coroutine _WaitToShoot;
        // Variables
        private TriggersDelegates _MyTriggerDetection;

        void Awake()
        {
            _Animator = GetComponent<Animator>();
            _StateMachine = GetComponent<NewFiniteStateMachine>();
            _Scriptable = GetComponent<Enemy>()._Scriptable;
            _MyTriggerDetection = transform.Find("AtackTrigger").GetComponent<TriggersDelegates>();
        }
        public override void Init()
        {
            _Animator.Play("Shoot");
            _MyTriggerDetection.OnTriggerStayDelegate += TriggerStay;
            _MyTriggerDetection.OnTriggerExitDelegate += TriggerExit;
            _Player = GetComponent<Enemy>()._Player;
            _WaitToShoot = StartCoroutine(WaitToShot());
        }

        /// <summary>
        /// Enemy wait a specific time before shooting.
        /// </summary>
        /// <returns> Wait 0.5f seconds </returns>
        private IEnumerator WaitToShot()
        {
            while (true)
            {
                yield return new WaitForSeconds(_Scriptable.AtackRecoy);
                if (IEnemyFunctions.EnemyShoot(gameObject, _Player))
                    if (_Player != null)
                        _Scriptable.RaiseWhenPlayerHit.Raise();
            }
        }

        /// <summary>
        /// OnTriggerStay of Detection Trigger
        /// </summary>
        /// <param name="other"> The Collider of the OnTriggerStay() </param>
        private void TriggerStay(Collider other)
        {
            if (other.gameObject.layer.Equals(LayerMask.NameToLayer("Player")))
            {
                RaycastHit hit;
                if (Physics.Raycast(transform.position, (other.transform.position - transform.position).normalized, out hit, Mathf.Infinity, LayerMask.GetMask("Player", "Obstacles")))
                    if (hit.transform.gameObject.layer != LayerMask.NameToLayer("Player"))
                        _StateMachine.ChangeState<SM_EnemyChasing>();
            }
        }
        /// <summary>
        /// OnTriggerExit of Detection Trigger
        /// </summary>
        /// <param name="other"> The Collider of the OnTriggerExit() </param>
        private void TriggerExit(Collider other)
        {
            if (other.gameObject.layer.Equals(LayerMask.NameToLayer("Player")))
            {
                StopAllCoroutines();
                _StateMachine.ChangeState<SM_EnemyChasing>();
            }
        }
        public override void Exit()
        {
            StopAllCoroutines();
            if (_MyTriggerDetection != null)
            {
                _MyTriggerDetection.OnTriggerExitDelegate -= TriggerExit;
                _MyTriggerDetection.OnTriggerStayDelegate -= TriggerStay;
            }
        }
    }
}
