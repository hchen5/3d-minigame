using System.Collections;
using System.Collections.Generic;
using FSMState;
using UnityEngine;
using UnityEngine.AI;
using static NS_Enemy.IEnemyFunctions;

namespace NS_Enemy
{
    public class SM_EnemyPatrol : MBState
    {
        // Components
        private NewFiniteStateMachine _StateMachine;
        private NavMeshAgent _NavMeshAgent;
        private Animator _Animator;
        // SO
        private SO_Enemy _Scriptable;
        // Variables
        private List<Vector3> _MyIteratorPath;
        private Coroutine _PlayerInTriger;
        private TriggersDelegates _MyTriggerDetection;
        private void Awake()
        {
            _Animator = GetComponent<Animator>();
            _NavMeshAgent = GetComponent<NavMeshAgent>();
            _StateMachine = GetComponent<NewFiniteStateMachine>();
            _Scriptable = GetComponent<Enemy>()._Scriptable;
            _MyIteratorPath = new List<Vector3>();
            _MyTriggerDetection = transform.Find("DetectionTrigger").GetComponent<TriggersDelegates>();
        }
        public override void Init()
        {
            _Animator.Play("Walk");
            _MyTriggerDetection.OnTriggerEnterDelegate += TriggerEnter;
            _MyTriggerDetection.OnTriggerExitDelegate += TriggerExit;
            GetComponent<Enemy>().WhenStopped += NextWayPoint;
            LookTheNearestPoint();
            NextWayPoint();
        }

        /// <summary>
        /// Enemy Search the New Destination Point and Go for it
        /// </summary>
        private void NextWayPoint()
        {
            if (_Scriptable.MyType.Equals(SO_Enemy.PatrolingType.Determinated))
            {
                if (_MyIteratorPath.Count == 0)
                    _MyIteratorPath = new List<Vector3>(_Scriptable.MyPath);
                Vector3 MyWaypoint = _MyIteratorPath[0];
                _MyIteratorPath.RemoveAt(0);
                IEnemyFunctions.EnemyMove(gameObject, MyWaypoint);
            }
            else if (_Scriptable.MyType.Equals(SO_Enemy.PatrolingType.Random))
            {
                Vector3 Result;
                IEnemyFunctions.NavMeshRandomPoint(_NavMeshAgent.areaMask, gameObject.transform.position, _Scriptable.radius, out Result);
                IEnemyFunctions.EnemyMove(gameObject, Result);
            }
            GetComponent<Enemy>().StartCheckingEnemyNavmeshPathStatus();
        }

        /// <summary>
        /// OnTriggerEnter of Detection Trigger
        /// </summary>
        /// <param name="other"> The Collider of the OnTriggerEnter() </param>
        private void TriggerEnter(Collider other)
        {
            if (other.transform.name == "Player")
                _PlayerInTriger = StartCoroutine(TriggerStay(other));
        }
        /// <summary>
        /// OnTriggerExit of Detection Trigger
        /// </summary>
        /// <param name="other"> The Collider of the OnTriggerExit() </param>
        private void TriggerExit(Collider other)
        {
            if (_PlayerInTriger != null && other.transform.name == "Player")
                StopCoroutine(_PlayerInTriger);
        }

        /// <summary>
        /// While Player is inside the trigger check with raycast if is visible and if so change the State
        /// </summary>
        /// <param name="other"> The Collider of the OnTriggerEnter() </param>
        /// <returns> Wait 0.3f Seconds </returns>
        private IEnumerator TriggerStay(Collider other)
        {
            while (true)
            {
                RaycastHit hit;
                if (Physics.Raycast(transform.position, (other.transform.position - transform.position).normalized, out hit, Mathf.Infinity, LayerMask.GetMask("Player", "Obstacles")))
                {
                    if (hit.transform.gameObject.layer == LayerMask.NameToLayer("Player"))
                    {
                        GetComponent<Enemy>()._Player = other.gameObject;
                        _StateMachine.ChangeState<SM_EnemyChasing>();
                    }
                }
                yield return new WaitForSeconds(0.2f);
            }
        }

        /// <summary>
        /// Look the nearest point of the navmesh in the path and begin the patrol with that point
        /// </summary>
        /// <returns> If the path is set </returns>
        private bool LookTheNearestPoint()
        {
            if (_Scriptable.MyType.Equals(SO_Enemy.PatrolingType.Determinated))
            {
                Vector3 SelectPoint = new Vector3(0,0,0);
                float minDistance = float.MaxValue;
                foreach (Vector3 Point in _Scriptable.MyPath)
                {
                    float distance = 0f;
                    NavMeshPath _MyNavPath = new NavMeshPath();
                    NavMesh.CalculatePath(transform.position, Point, _NavMeshAgent.areaMask, _MyNavPath);
                    for (int i = 0; i < _MyNavPath.corners.Length - 2; i++)
                        distance += (_MyNavPath.corners[i + 1] - _MyNavPath.corners[i]).magnitude;
                    if (minDistance > distance)
                    {
                        minDistance = distance;
                        SelectPoint = Point;
                    }
                }
                _MyIteratorPath = new List<Vector3>(_Scriptable.MyPath);
                foreach (Vector3 Point in _Scriptable.MyPath)
                {
                    if (Point == SelectPoint)
                        break;
                    else
                        _MyIteratorPath.RemoveAt(0);
                }
                return true;
            }
            return false;
        }
        
        public override void Exit()
        {
            if (_MyTriggerDetection != null)
            {
                _MyTriggerDetection.OnTriggerEnterDelegate -= TriggerEnter;
                _MyTriggerDetection.OnTriggerExitDelegate -= TriggerExit;
            }
            StopAllCoroutines();
            GetComponent<Enemy>().WhenStopped -= NextWayPoint;
            GetComponent<Enemy>().StopCheckingEnemyNavmeshPathStatus();
            IEnemyFunctions.EnemyStop(gameObject);
        }
    }
}

