using System;
using System.Collections;
using System.Collections.Generic;
using FSMState;
using UnityEngine;
using static NS_Enemy.IEnemyFunctions;

namespace NS_Enemy
{
    public class SM_EnemyIddle : MBState
    {
        // Components
        private NewFiniteStateMachine _StateMachine;
        // SO
        private SO_Enemy _Scriptable;
        private void Awake()
        {
            _StateMachine = GetComponent<NewFiniteStateMachine>();
            _Scriptable = GetComponent<Enemy>()._Scriptable;
        }
        public override void Init()
        {
            
        }
    }
}
