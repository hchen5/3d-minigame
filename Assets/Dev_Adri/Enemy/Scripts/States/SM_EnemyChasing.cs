using System;
using System.Collections;
using System.Collections.Generic;
using FSMState;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.AI;
using static NS_Enemy.IEnemyFunctions;

namespace NS_Enemy
{
    public class SM_EnemyChasing : MBState
    {
        // Components
        private NewFiniteStateMachine _StateMachine;
        private NavMeshAgent _NavMeshAgent;
        private Animator _Animator;
        // SO
        private SO_Enemy _Scriptable;
        // Variables
        private TriggersDelegates _MyTriggerDetection;
        private GameObject _Player;
        private Coroutine _ChaseCoroutine;
        void Awake()
        {
            _Animator = GetComponent<Animator>();
            _NavMeshAgent = GetComponent<NavMeshAgent>();
            _StateMachine = GetComponent<NewFiniteStateMachine>();
            _Scriptable = GetComponent<Enemy>()._Scriptable;
            _MyTriggerDetection = transform.Find("AtackTrigger").GetComponent<TriggersDelegates>();
        }
        public override void Init()
        {
            _Animator.Play("Run");
            _NavMeshAgent.speed *= _Scriptable.VelocityIncrement;
            _Player = GetComponent<Enemy>()._Player;
            _MyTriggerDetection.OnTriggerStayDelegate += TriggerStay;
            GetComponent<Enemy>().WhenStopped += ReturnToPatrol;
            GetComponent<Enemy>().StartCheckingEnemyNavmeshPathStatus();
            _ChaseCoroutine = StartCoroutine(Chasing());
        }

        /// <summary>
        /// OnTriggerStay of Detection Trigger
        /// </summary>
        /// <param name="other"> The Collider of the OnTriggerStay() </param>
        private void TriggerStay(Collider other)
        {
            if (other.gameObject.name == "Player")
            {
                RaycastHit hit;
                if (Physics.Raycast(transform.position, (other.transform.position - transform.position).normalized, out hit, Mathf.Infinity, LayerMask.GetMask("Player", "Obstacles")))
                    if (hit.transform.gameObject.layer == LayerMask.NameToLayer("Player"))
                        _StateMachine.ChangeState<SM_EnemyAtack>();
            }
        }

        /// <summary>
        /// Check Player with raycast and if is visible chase him
        /// </summary>
        /// <returns> Wait 0.5f seconds </returns>
        private IEnumerator Chasing()
        {
            IEnemyFunctions.EnemyMove(gameObject, _Player.transform.position);
            while (true)
            {
                RaycastHit hit;
                if (Physics.Raycast(transform.position, (_Player.transform.position - transform.position).normalized, out hit, Mathf.Infinity, LayerMask.GetMask("Player", "Obstacles")))
                {
                    if (hit.transform.gameObject.layer == LayerMask.NameToLayer("Player"))
                        IEnemyFunctions.EnemyMove(gameObject, _Player.transform.position);
                }
                yield return new WaitForSeconds(0.5f);
            }
        }

        /// <summary>
        /// Change the enemy State to Patroling
        /// </summary>
        private void ReturnToPatrol()
        {
            _StateMachine.ChangeState<SM_EnemyPatrol>();
        }

        public override void Exit()
        {
            StopAllCoroutines();
            GetComponent<Enemy>().StopCheckingEnemyNavmeshPathStatus();
            if (_MyTriggerDetection != null)
                _MyTriggerDetection.OnTriggerStayDelegate -= TriggerStay;
            GetComponent<Enemy>().WhenStopped -= ReturnToPatrol;
            IEnemyFunctions.EnemyStop(gameObject);
            if (_NavMeshAgent != null)
                _NavMeshAgent.speed /= _Scriptable.VelocityIncrement;
        }
    }
}
