using System.Collections;
using System.Collections.Generic;
using FSMState;
using NS_Enemy;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.AI;

public class EnemySpawner : MonoBehaviour
{
    void Start()
    {
        for (int i = 0; i < GetComponent<Pool>().Capacitat; i++)
            Spawn();
    }
    private void Spawn()
    {
        GameObject spw = GetComponent<Pool>().GetElement();
        spw.GetComponent<Enemy>().WhenDied -= Spawn;
        spw.GetComponent<Enemy>().WhenDied += Spawn;
        spw.transform.localPosition = Vector3.zero;
        spw.GetComponent<NewFiniteStateMachine>().ChangeState<SM_EnemyPatrol>();
        spw.GetComponent<NavMeshAgent>().isStopped = false;
        spw.transform.GetComponent<CapsuleCollider>().enabled = true;
        spw.GetComponent<Animator>().enabled = true;
    }
}
